# Information
This is a simple weather app created using React JS with Bulma CSS and Openweathermap as the backend.

# Running the app
```
// Clone the app to your local drive
git clone https://gitlab.com/AizenSousuke/weather
// Go into the newly created folder
cd <newly created folder>
// Install the app's dependencies (Make sure you have NPM installed)
npm install
// Start the app in development mode
npm start
```
Go to https://localhost:3000 on your browser to view the app.

# How to use the App
You can add a new City to a list by inputting a city name in the input box, setting the interval if you wish to do so, and press the Create button.

A max of 3 cities can be viewed at one time. 

Remove the city by hovering over its container and clicking on the red remove button that will appear. 