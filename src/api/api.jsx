import axios from 'axios';

const apiUrl = `https://api.openweathermap.org/data/2.5/weather?q=`;
const apiKey = `&appid=f713e61901ef1d2c283e91271e47ced1`;

export const getWeatherDataByCityName = async (city) => {
    let data = await axios.get(apiUrl.toString() + city.toString() + apiKey.toString())
    .then((response) => {
        console.log("GET api");
        return response.data;
    });
    return data;
}
