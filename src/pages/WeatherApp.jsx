import React, { Component } from "react";
import WeatherTile from "../components/WeatherTile";
import { getWeatherDataByCityName } from "../api/api";
// Sample url: https://api.openweathermap.org/data/2.5/weather?q=Singapore&appid=f713e61901ef1d2c283e91271e47ced1

export default class WeatherApp extends Component {
	constructor(props) {
		super(props);
		this.state = {
			shown: 0,
			maxShown: 3,
			interval: 2,
			input: "singapore",
			city: [],
			id: 0,
		};
		this.AddTile.bind(this);
		this.handleRemove.bind(this);
		this.handleInterval.bind(this);
	}

	async AddTile(city) {
		this.setState(
			{ id: this.state.id + 1, shown: this.state.shown + 1 },
			() => {
				console.log(this.state.shown);
			}
		);
		let newCity = await getWeatherDataByCityName(city ?? "singapore");
		let newCityData = {
			id: this.state.id,
			data: newCity,
			interval: parseInt(this.state.interval),
		};
		this.setState((prevState) => ({
			city: [...prevState.city, newCityData],
		}), () => {
			console.log(this.state.city);
		});
	}

	handleInputChange = (e) => {
		this.setState({ input: e.target.value });
	};

	handleInterval = (e) => {
		// console.log("Changed interval: " + e.target.value);
		this.setState({ interval: e.target.value }, () => {
			console.log("Interval has been set to: " + this.state.interval);
		});
	};

	handleRemove(id) {
		// console.log("Removing Event " + id);
		// var index = this.state.city.findIndex((city) => city.id === id);
		// console.log("Index: " + index);
		this.setState(
			(prevState) => ({
				city: [...prevState.city.filter((city) => city.id !== id)],
			}),
			() => {
				this.setState({
					shown: this.state.shown - 1,
				});
				// console.log(this.state.shown);
			}
		);
	}

	async updateData(city) {
		// console.log("=======================");
		// console.log(this.state.city);
		// console.log("Updating data for " + city.id + " - " + city.data.name);
		var index = this.state.city.findIndex((c) => c.id === city.id);
		// console.log("Index: ", index);
		let newCity = await getWeatherDataByCityName(city.data.name);
		let newCityData = {
			id: city.id,
			data: newCity,
			interval: city.interval,
		};
		// console.log(newCityData);
		// var updatedCity = update(this.state.city, {[index]: newCityData});
		// var newCityArray = [...this.state.city];
		// var updatedCity = [newCityArray.splice(index, 1, newCityData)];
		const updatedCity = Array.from(this.state.city);
		updatedCity[index] = newCityData;
		console.log(updatedCity);
		this.setState((prevState) => ({
			city: updatedCity,
		}));
	}

	render() {
		return (
			<div className="">
				<div className="title header">Weather App</div>
				<div className="columns is-mobile">
					{this.state.city.map((city) => {
						// console.log("Mapping : " + city.data.name);
						return (
							// <>
							<WeatherTile
								key={city.id}
								city={city}
								removeEvent={() => this.handleRemove(city.id)}
								updateData={() => {this.updateData(city)}}
							/>
							// </>
						);
					})}
				</div>
				<div className="columns is-mobile">
					<div className="column"></div>
					<div className="column">
						<div>
							<span>
								<div>Select interval:</div>
								<div className="select">
									<select
										onChange={this.handleInterval}
										value={this.state.interval}
									>
										<option defaultValue={true} value={2}>
											2s
										</option>
										<option value={5}>5s</option>
										<option value={10}>10s</option>
									</select>
								</div>
							</span>
						</div>
						{this.state.shown < this.state.maxShown && (
							<>
								<div>
									<input
										className="input is-primary"
										type="text"
										placeholder="Input a city name here"
										value={this.state.input}
										onChange={this.handleInputChange}
									/>
								</div>
								<div
									className="button is-primary"
									onClick={() =>
										this.AddTile(this.state.input)
									}
								>
									Create
								</div>
							</>
						)}
						{this.state.shown >= this.state.maxShown && (
							<>
								Max number has been shown. Delete some.
							</>
						)}
					</div>
					<div className="column"></div>
				</div>
			</div>
		);
	}
}
