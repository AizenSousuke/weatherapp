import React from "react";
import "./App.css";
import WeatherApp from "./pages/WeatherApp";

function App() {
	return (
		<div className="App">
			<div className="column"></div>
			<div className="column">
				<WeatherApp />
			</div>
			<div className="column"></div>
		</div>
	);
}

export default App;
