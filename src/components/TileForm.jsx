import React, { Component } from 'react'

export default class TileForm extends Component {
    render() {
        return (
            <div className="container is-centered is-half-desktop">
                Tile Form
                <form>
                    <div>
                        <input className="input is-primary" type="text" name="city" id="city" placeholder="Input a city name here" />
                    </div>
                    <div className="select">
                        <select>
                            <option>Select interval</option>
                            <option>5s</option>
                            <option>30s</option>
                            <option>60s</option>
                        </select>
                    </div>
                    <div>
                        <button className="button is-primary" type="submit">Create</button>
                    </div>
                </form>
            </div>
        )
    }
}
