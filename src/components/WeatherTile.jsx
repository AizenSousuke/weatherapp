import React, { useState, useEffect } from "react";

const WeatherTile = (props) => {
	const [city, setCity] = useState(props.city.data);
	const [remove, setRemove] = useState(false);
	const [hover, setHover] = useState("hidden");
	const [updateHelper, setUpdateHelper] = useState(0);

	// Delete
	useEffect(() => {
		if (remove) {
			console.log("Deleting component");
			props.removeEvent();
		}
	});

	// Interval for refresh
	useEffect(() => {
		// console.log(props.city.data.name + " interval is: " + props.city.interval)
		const interval = setInterval(() => {
			console.log("Refreshed " + city.name);
			console.log("Interval " + props.city.interval);

			// Send event to parent to refresh the data here
			props.updateData();
			setUpdateHelper(updateHelper + 1);
		}, props.city.interval * 1000);
		return () => {
			clearInterval(interval);
		};
	}, [props.city, city.name, updateHelper, props.interval]);

	return (
		<div className="column">
			<div
				className="card"
				onMouseEnter={() => setHover("visible")}
				onMouseLeave={() => setHover("hidden")}
			>
				<div className="card-header">
					<div className="column">
						<div className="title">{city.name}</div>
						<div className="subtitle">
							Refresh interval: {props.city.interval}
						</div>
						<div className="subtitle">
							Updated {updateHelper} times.
						</div>
					</div>
				</div>
				<div className="card-image is-rounded">
					<figure className="image">
						<img
							src={`http://loremflickr.com/500/500/${city.weather[0].main}`}
							alt={city.weather[0].main}
						/>
					</figure>
				</div>
				<div className="card-content">
					<button
						className="button is-danger is-small"
						style={{ visibility: hover }}
						onClick={() => {
							setRemove(true);
						}}
					>
						Remove
					</button>
				</div>
				<div className="card-footer columns">
					<div className="column">
						<div className="columns">
							Weather Data (temperature, pressure, humidity)
							<div className="column">
								Temperature {city.main.temp}
							</div>
							<div className="column">
								Pressure {city.main.pressure}
							</div>
							<div className="column">
								Humidity {city.main.humidity}
							</div>
						</div>
					</div>
					<div className="column">
						Weather:
						<div className="is-uppercase">
							{city.weather[0].main}
						</div>
						<div className="is-small">
							Description: {city.weather[0].description}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default WeatherTile;
